try:
    from setting_local import *
except ImportError:
    print('use configuration file from setting.py')
    USER = 'user'
    PASS = 'pass'
    WSDL = 'https://www.g-nav.org/gnav/webService/osrt.php?wsdl'
    PLANE = 'F-CMJL'
