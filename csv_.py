from collections import namedtuple
from base64 import b64encode
import setting
from colored import fg, attr


def create_activity(file_path):
    """
    parse a csv file and return a raw b64 encoded xml
    :param file_path: csv file path
    :return tuple: (nb_record(int), nb_activity(int), data(str))
    data is a b64 encoded raw xml build on csv file and format as follow:
    <?xml version="1.0" encoding="UTF-8"?>
    <activite>
        <aeronef immat="F-CHFR" date="20090818" hvol="3:55" remo="0" treuil="5" auto="0" hmot="0" crochet="5"></aeronef>
        <aeronef immat="F-CHFR" date="20090817" hvol="4:50" remo="1" treuil="1" auto="0" hmot="0" crochet="2"></aeronef>
        <...>
    </activite>
    """
    activity_skel = '<?xml version="1.0" encoding="UTF-8"?>\n' \
                    '<activite>\n{}\n</activite>'

    activity_line = '<aeronef immat="{}" date="{}" ' \
                    'hvol="{}" remo="1" treuil="0" auto="0" ' \
                    'hmot="0" crochet="1"></aeronef>'

    activity_lines = list()

    Csv = namedtuple("csv", ["date", "tof_hms", "plane", "function", "tof_type", "duration",
                             "tof_oaci", "l_oaci", "tof_nb"])

    nb_line = 0
    try:
        with open(file_path, 'r', encoding="iso-8859-1") as file:
            for line_ in file.readlines():
                nb_line += 1
                line = line_.rstrip()
                # avoid processing csv header file
                if nb_line == 1:
                    continue
                # export file is full of '"', delete it.
                line = line.replace('"','')
                fields = line.split(';')[:-1]
                csv = Csv(*fields)
                if setting.PLANE in csv.plane or (csv.plane == '' and csv.tof_oaci == 'LFNF'):
                    print('{}[X] {}{}'.format(fg('green'), line, attr(0)))
                    date = csv.date.replace('-', '')
                    aline = activity_line.format(setting.PLANE, date, csv.duration)
                    activity_lines.append(aline)
                else:
                    print('{}[ ] {}{}'.format(fg('red'), line, attr(0)))
    except BaseException as err:
        print('create_activity error: {}'.format(err))

    activity = activity_skel.format('\n'.join(activity_lines))
    data = str(b64encode(activity.encode()), 'utf8')
    return nb_line-1, len(activity_lines), data
