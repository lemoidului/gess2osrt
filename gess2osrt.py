from csv_ import create_activity
from zeep import Client
import sys
import setting
from colored import fg, attr

"""
From: https://documentation.g-nav.org/index.php/Service_Web
Soap: https://gnav.g-nav.org/webService/osrt.php?WSDL#
"""


def send_activity(rawdata):
    """
    send rawdata b64 encoded xml to soap service
    :param rawdata: str
    :return:
    """
    soap = {'aeronefActivite': {
        'code_gnav': setting.USER,
        'mot_de_passe': setting.PASS,
        'data': rawdata}}

    try:
        client = Client(setting.WSDL)
        resp = client.service.setAeronefActivite(**soap)
        print(str(resp.encode('iso-8859-1'), "utf-8"))
    except BaseException as err:
        print("{}API error: {}{}".format(fg('red'), err, attr(0)))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: python3 {} csv_file'.format(sys.argv[0]))
        exit(0)

    nb_records, nb_activity, data = create_activity(sys.argv[1])
    print('------------------------------')
    print('{} records extract, {} activity found'.format(nb_records, nb_activity))
    if nb_activity == 0:
        exit(0)
    response = input('send to service ?\n(type yes, default no): ')
    if response.lower() == 'yes':
        send_activity(rawdata=data)
