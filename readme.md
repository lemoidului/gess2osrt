# Gess2osrt
Small python script for parsing csv file from Geasso website: https://gesasso.ffvv.stadline.com/login  
After user confirm, send flight activities on GNAV-OSRT website via a soap Api: https://documentation.g-nav.org/index.php/Service_Web

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Python version >= 3.5

### Installing
Create a new Python venv

```
python3 -m venv ./venv
or
virtualenv -p python3 ./venv
```
activate your venv

```
source venv/bin/activate
```
Install Dependencies
```
pip install -r requirements.txt
```

### Running
```
Edit setting.py with osrt_user, osrt_pass and your Glider immat
use the script:
(venv) python gess2osrt.py export.csv
```

### Using Docker


Build Image (based on Alpine with prebuilt wheel tarball)
```
docker build -t osrt -f Docker/Dockerfile .
```
Edit setting.py file and run container
```
docker run -it --rm -v $(pwd):/app osrt /bin/sh
```
Use Application
```
/app # python gess2osrt.py export.csv 
```



### License
GNU GENERAL PUBLIC LICENSE v.3  
A copy of License is joined to the repo